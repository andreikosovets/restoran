
function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 55.798915, lng: 37.403297},
        scrollwheel: false,
        zoom: 16
    });
    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: {lat: 55.798915, lng: 37.403297},
        //title: 'Hello World!'
    });
}

$('.bxslider').length ? $('.bxslider').bxSlider({
    pager:false
}) : "";